# KML Writter

![npm](https://img.shields.io/npm/dw/kmlwriter.svg?logoColor=green)
![NPM](https://img.shields.io/npm/l/kmlwriter.svg)

## Introduction


KML writing javascript librari

## Dependences




## Installation
```sh
$ npm i kmlwriter
```
## Functions

## KML operations

### Start a kml

Generate a new KML file:

Will start the kml object. We can define a name for the kml, if we don't define it, will saved as kml by default.

```javascript
kml = new kmlWriter()
kml.startKml(name = 'kml')
```

## Placemark operations

### Generate a Placemark

```js
addPlacemark(id,name,lon,lat,range, altMode = 'relativeToGround', description = '', icon= '' )
```
### edit the coordinates of a Placemark

```js
editCoodPlacemark(id, lon, lat, range)
```

## Tour Operations

### Create a Tour

```javascript
kmlWriter.createTour(ID, name,description))
```


### Generate KML file

The generete KML function will use the name of the kml defined when the kml is started.

```javascript
kmlWriter.saveKML('path/to/file')
```

### Create a NetworkLink

the functions needs a id, to set it into the tag, and the ip or link to our server.

```javascript
kml.networkLink(id, link, fly = 0, visibility = 1 )
```

## Find errors:

in editCoodPlacemark if didn't find the index crush.

### Colaborators:

Renato Fernandes

* GitLab account: [@renatoaurefer](https://gitlab.com/renatoaurefer)


Eric Monné Mesalles

* GitLab account: [@xemyst](https://gitlab.com/Xemyst)

Letícia Vigna

* GitLab account [@letvigna](https://gitlab.com/letvigna)
