//Reqs
const fs    = require('fs');
var jsonxml = require('jsontoxml')
class kmlWriter {

    /**
     *
     * @param {string} type the encoding used to write the kml file, defaults do utf-8
     */
    constructor(type = "utf-8") {
        this.encoding = type
        this.file = null
        this.kml = {  }
    }

    startKml(name="kml",visibility=1) {
      this.name = name
      this.kml = {
        Folder: []
       }
    }



    /*****
    Tour Operation
    *****/

    createTour(ID, name,description){
        this.buildTour(ID, name,description)
    }

    buildTour(ID, name, description){

      var tour = {
        name: 'gx:Tour',
        attrs:{
        'id' : ID,
        },
        children: [
          {name: 'name', text: name},
          {name: 'description', text:description},
          {name: 'gx:Playlist', children: [] }
        ]
      }
      this.kml.Folder.push(tour)
    }

    addIntoTour(id, lon,lat, alt, head, tilt, altMode){
      var index = this.getTagById('gx:Tour',id)
      this.kml.Folder[index].children[2].children.push(this.generateFlyTo(lon,lat,alt,head,tilt,altMode))

    }

    createOrbit(id,name,description,latitude,longitude,range){
      this.createTour(id,name,description)
      var heading = 0
      for(var x = 0; x < 300; x++){
        this.addIntoTour(id,longitude,latitude,range,heading,58,'relativeToGround')
        heading = (heading + 10) % 360
      }
    }


    generateFlyTo(lon,lat,range,head,tilt,altMode = 'relativeToGround',duration =2 ){
    var data =  {
      name: 'gx:FlyTo',
      children: [
        {name: 'gx:flyToMode', text: 'smooth'},
        {name: 'gx:duration', text: '1'}
      ]
    }
    data.children.push(this.generateVisionTag('LookAt',lon,lat,range,head,tilt,altMode))
    return data
    }


    //generate Placemarks
    addPlacemark(id,name,lon,lat,range, altMode = 'relativeToGround', description = '', icon= '',scale ){
        var data  = this.generatePlacemark(id, name, lon, lat, range,altMode,description, icon,scale)
        this.kml.Folder.push(data)
    }

    deleteTagById(tag,id){
      var index = this.getTagById(tag, id)
      if(index != undefined){
        this.kml.Folder.splice(index,1)
      }
    }



    editBalloonState(id, newValue){
      var index = this.getTagById('Placemark', id)
      this.kml.Folder[index].children[2].text = newValue
    }

    editCoodPlacemark(id, lon, lat, range ){
      var index = this.getTagById('Placemark', id)

      // if( index === null ){
      //   console.log("out")
      //   return false
      // }
      // this.kml.Folder[index].children[5].children[0].text = lat + ',' + lon
      // this.kml.Folder[index].children[4].children[0].text = lon
      this.kml.Folder[index].children[4].children[0].text = lat + ',' + lon + ',' + range
    }

    addGroundOverlay(id,name,url,firstCorner,secondCorner,thirdCorner,fourthCorner){
      var corners = firstCorner + " " + secondCorner +" " + thirdCorner + " " + fourthCorner
      var data = {
        name          : 'GroundOverlay',
        attrs         : {id : id},
        children      : [
          { name: 'name',           text: name },
          {name: 'Icon',
          children: [ {name: 'href', text: url } ]
        },
          { name: 'gx:LatLonQuad',
            children: [
              { name: 'coordinates',
                text: corners,
              },

            ]
          },
        ]
      }
      this.kml.Folder.push(data)

    }

    generatePlacemark(id, name, lon,lat,range,altMode, description, icon,scale){
      var data = {
        name          : 'Placemark',
        attrs         : {id : id},
        children      : [
          { name: 'name',           text: name },
          { name: 'description',    text: description},
          { name: 'gx:balloonVisibility', text: 0},
          { name: 'LookAt',
            children: [
              { name: 'longitude',  text: lon},
              { name: 'latitude',   text: lat},
              { name: 'range',      text:range},
            ]
          },
        ]
      }
      data.children.push(this.generatePoint(lon,lat,range, altMode))
      if( icon != 0 ){

        data.children.push({
          name: 'Style', attrs: {id: id+'icon'},
          children : [
            {
            name: 'IconStyle',
              children:[
                {
                  name: 'scale', text: scale
                },
                {name: 'Icon',
                  children: [
                    {name: 'href', text: icon}
                  ]
                }
              ]
            }
          ]
        })
        data.children.push({
          name: 'styleUrl', text: ('#' + id +'icon')
        })
      }
      return data
    }

  // network link creation
    networkLink(id, link, fly = 0, visibility = 1 ){

      var data = {
        name     : 'NetworkLink',
        children : [
          { name : 'Link',
            children : [
              { name : 'href'           , text : link },
              { name : 'refreshMode'    , text: 'onInterval'},
              { name : 'refreshInterval', text: '1 '}
            ]
          }
        ]
      }
      this.kml.Folder.push(data)

    }

    //
    //  Geometry functions
    //

    //build polygon

    createPolygon(id,name, extrude = 1){
      var data = {
        name     : 'Placemark',
        attrs     : {id:id},
        children : [
          { name : 'name'      , text: name },
          { name : 'Polygon', children: [
            {name:'extrude', text: 1},
            {name:'altitudeMode', text: 'relativeToGround'},

          ]}
        ]
      }
      this.kml.Folder.push(data)
    }


    addOuterBoundary(polygonId, coordinates){
      var index = this.getTagById('Placemark',polygonId)
      var data = {
        name     : 'outerBoundaryIs',
        children : [
          { name : 'LinearRing'      , children: [ {name : 'coordinates', text: coordinates} ] }
        ]
      }
      this.kml.Folder[index].children[1].children.push(data)

    }

    addInnerBoundary(polygonId, coordinates){
      var index = this.getTagById('Placemark','pentagon')
      var data = {
        name     : 'innerBoundaryIs',
        children : [
          { name : 'LinearRing'      , children: [ {name : 'coordinates', text: coordinates} ] }
        ]
      }
      this.kml.Folder[index].children[1].children.push(data)
    }


    generatePoint(lon, lat, range, altMode){
        return {
        name : 'Point',
        children : [{
          name : 'coordinates',
          text : lon + "," + lat + "," + range,
        },
        {
          name: 'altitudeMode',
          text: altMode
        }
        ]
      }
    }

    createLineString(id,name,coordinates,tessellate = 1){
      var data = {
        name     : 'Placemark',
        attrs     : {id:id},
        children : [
          { name : 'name'      , text: name },
          { name : 'LineString', children: [
            {name: 'altitudeMode', text:'relativeToGround'},
            {name: 'tessellate', text: tessellate},
            {name: 'coordinates', text: coordinates}
          ]}
        ]
      }
      this.kml.Folder.push(data)
    }




    //suport functions
    getTagById(name,id){
      var index
      this.kml.Folder.forEach(function(tag, i){
        if(tag.name == name && tag.attrs.id == id){
          index = i
          return
        }
      })
      if(index != undefined){
        return index
      }
     }

    //Camera, LookAt
    generateVisionTag(name,lon,lat,range,head,tilt,altMode){
      return {
        name: name,
        children: [
          {name: 'longitude', text: lon},
          {name: 'latitude' , text: lat},
          {name: 'range'    , text: range},
          {name: 'heading'  , text: head},
          {name: 'tilt'     , text: tilt},
          {name: 'altitudeMode' , text: altMode}
        ]}
    }


    //save operations with KML
    saveKML(path){
        var currentName = path + this.name + ".kml"
        var kml = this.getKML()
        var text = ("<kml xmlns=" + '"http://www.opengis.net/kml/2.2" ' +
        "xmlns:gx="+' "http://www.google.com/kml/ext/2.2">' + '<Document>')
        kml = kml.replace("<kml>",text)
        kml = kml.replace("</kml>",'</Document></kml>')
        return new Promise ((resolve,reject) => {
          fs.writeFile(currentName,kml, function (err) {
            if(err){
              return reject()
            }else{
              return resolve()
            }
          });

        })

    }
    getKML(){

      var data = this.kml
      var doc = {kml : data}
      return jsonxml(doc,'xmlHeader')
    }



}


module.exports = kmlWriter
