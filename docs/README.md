# KML Writter

![npm](https://img.shields.io/npm/dw/kmlwriter.svg?logoColor=green)
![NPM](https://img.shields.io/npm/l/kmlwriter.svg)


## Introduction

KML writing javascript library

## Dependences

## Installation
```sh
$ npm i kmlwriter
```
## Functions

### Create a KML

```javascript
kmlWriter.startKml()
```

### Read KML

Read an existing KML file:

```javascript
kmlWriter.readFile('path/to/file')
```
### Start a kml

Generate a new KML file:

```javascript
kmlWriter.startKml()
```
### Add a placemark

```javascript
kmlWriter.addPlaceMark(int Id, string Name, int StyleID,  longitud, latitude, range)
```
### Generate KML file

```javascript
kmlWriter.writeFile('path/to/file')
```

### Colaborators:

Renato Fernandes

* GitLab account: [@renatoaurefer](https://gitlab.com/renatoaurefer)


Eric Monné Mesalles

* GitLab account: [@xemyst](https://gitlab.com/Xemyst)

Letícia Vigna

* GitLab account: [@letvigna](https://gitlab.com/letvigna)
